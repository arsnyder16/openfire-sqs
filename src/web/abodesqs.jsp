<%@ page import="java.util.*,
                 org.jivesoftware.openfire.XMPPServer,
                 org.jivesoftware.util.*,
                 com.abode.openfire.AbodeSQSPlugin"
    errorPage="error.jsp"
%>

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt" %>

<jsp:useBean id="webManager" class="org.jivesoftware.util.WebManager"  />
<% webManager.init(request, response, session, application, out ); %>

<%  // Get parameters
    boolean save = request.getParameter("save") != null;
    boolean success = request.getParameter("success") != null;
    String awsAccessKey = ParamUtils.getParameter(request, "aws_access_key");
    String awsSecretKey = ParamUtils.getParameter(request, "aws_secret_key");
    String awsSqsQueueUrl = ParamUtils.getParameter(request, "aws_sqs_queue_url");
    String awsSqsQueueEndpoint = ParamUtils.getParameter(request, "aws_sqs_queue_endpoint");
    String username = ParamUtils.getParameter(request, "username");
    boolean enabled = ParamUtils.getBooleanParameter(request, "enabled");

    AbodeSQSPlugin plugin = (AbodeSQSPlugin) XMPPServer.getInstance().getPluginManager().getPlugin("abodesqs");

    // Handle a save
    Map errors = new HashMap();
    if (save) {
       if (errors.size() == 0) {
           plugin.setAWSAccessKey(awsAccessKey);
           plugin.setAWSSecretKey(awsSecretKey);
           plugin.setAWSSQSQueueUrl(awsSqsQueueUrl);     
           plugin.setAWSSQSQueueEndpoint(awsSqsQueueEndpoint);      
           plugin.setUsername(username);     
           plugin.setEnabled(enabled);
           response.sendRedirect("abodesqs.jsp?success=true");
           return;
       }
    }

    awsAccessKey = plugin.getAWSAccessKey();
    awsSecretKey = plugin.getAWSSecretKey();
    awsSqsQueueUrl = plugin.getAWSSQSQueueUrl();
    awsSqsQueueEndpoint = plugin.getAWSSQSQueueEndpoint();
    username = plugin.getUsername();
    enabled = plugin.isEnabled();
%>

<html>
    <head>
        <title>SQS Properties</title>
        <meta name="pageID" content="abodesqs"/>
    </head>
    <body>
    
<%  if (success) { %>

    <div class="jive-success">
    <table cellpadding="0" cellspacing="0" border="0">
    <tbody>
        <tr><td class="jive-icon"><img src="images/success-16x16.gif" width="16" height="16" border="0"></td>
        <td class="jive-icon-label">
            Properties edited successfully.
        </td></tr>
    </tbody>
    </table>
    </div><br>
<% } %>

<form action="abodesqs.jsp?save" method="post">

<fieldset>
    <legend>SQS</legend>
    <div>
	    <ul>
	        <input type="radio" name="enabled" value="true" id="rb01"
	        <%= ((enabled) ? "checked" : "") %>>
	        <label for="rb01"><b>Enabled</b> - Messages will be sent to Amazon SQS.</label>
	        <br>
	        <input type="radio" name="enabled" value="false" id="rb02"
	         <%= ((!enabled) ? "checked" : "") %>>
	        <label for="rb02"><b>Disabled</b> - Messages will have default behaviour.</label>
	        <br><br>
	
	        <label for="text_secret">AWS Access Key:</label>
	        <input type="text" name="aws_access_key" value="<%= awsAccessKey %>" id="text_access" size="50">
	        <br><br>
	
	        <label for="text_secret">AWS Secret Key:</label>
	        <input type="text" name="aws_secret_key" value="<%= awsSecretKey %>" id="text_secret" size="50">
	        <br><br>
	        
	        <label for="text_secret">AWS Endpoint:</label>
	        <input type="text" name="aws_sqs_queue_endpoint" value="<%= awsSqsQueueEndpoint %>" id="text_endpoint" size="50">
	        <br><br>
	
	        <label for="text_secret">AWS SQS Queue URL:</label>
	        <input type="text" name="aws_sqs_queue_url" value="<%= awsSqsQueueUrl %>" id="text_queue" size="50">
	        <br><br>
	        
	        <label for="text_secret">Username:</label>
	        <input type="text" name="username" value="<%= username %>" id="text_queue" size="50">
	        <br><br>
	    </ul>
    </div>
</fieldset>

<br><br>

<input type="submit" value="Save">
</form>


</body>
</html>