package com.abode.openfire;

import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.interceptor.PacketInterceptor;
import org.jivesoftware.openfire.interceptor.PacketRejectedException;
import org.jivesoftware.openfire.session.Session;
import org.jivesoftware.util.EmailService;
import org.jivesoftware.util.JiveGlobals;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmpp.packet.JID;
import org.xmpp.packet.Message;
import org.xmpp.packet.Packet;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.util.json.Jackson;

public class AbodeSQSInterceptor implements PacketInterceptor {
	
	private Logger log;
	private AmazonSQS mSqs;
	private String url;
	private String username;
	
	public AbodeSQSInterceptor(String accessKey,String secretKey,String queueEndpoint, String queueUrl,String user) {
		
		log = LoggerFactory.getLogger(AbodeSQSInterceptor.class);
		mSqs = new AmazonSQSClient(new BasicAWSCredentials(accessKey, secretKey));
		mSqs.setEndpoint(queueEndpoint);
		url = queueUrl;
		username = user;
	}
	
	  public void interceptPacket(Packet packet, Session session, boolean read, boolean processed) throws PacketRejectedException {	
		  if (log.isDebugEnabled() && packet != null) {
          	log.debug("intercepted packet:"  + (packet.getTo() == null ? "" : packet.getTo().toString() + " " + packet.getTo().getNode() + " " + username+ " " + processed+ " " + read+ " " + packet.getClass().getName()));
          }	
	        if (!processed && read && (packet instanceof Message) && 
	        		packet.getTo() != null && packet.getTo().getNode().equalsIgnoreCase(username)) {           
	            if(sendToSQS((Message)packet))
	            {
	            	throw new PacketRejectedException("packet successfully sent to SQS");
	            }
	        }
	    }


	private Boolean sendToSQS(Message message) {
			try {
				mSqs.sendMessage(new SendMessageRequest(url,String.format("{to:%s,from:%s,message:%s}",
								message.getTo().toBareJID(),
								message.getFrom().toBareJID(),
								message.getBody())));
				return true;
				
			} catch (AmazonServiceException ase) {
				log.error(String.format("AmazonServiceException: %s Error Code: %s",ase.getMessage(),ase.getErrorCode()), ase);
	        } catch (AmazonClientException ace) {
	        	log.error("AmazonClientException: " + ace.getMessage(), ace);
	        } 		
			return false;
	}
}

