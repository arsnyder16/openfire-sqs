package com.abode.openfire;

import java.io.File;
import java.util.Map;

import org.jivesoftware.openfire.container.Plugin;
import org.jivesoftware.openfire.container.PluginManager;
import org.jivesoftware.openfire.interceptor.InterceptorManager;
import org.jivesoftware.util.JiveGlobals;
import org.jivesoftware.util.PropertyEventDispatcher;
import org.jivesoftware.util.PropertyEventListener;

public class AbodeSQSPlugin implements Plugin, PropertyEventListener {

	private String awsAccessKey;
	private String awsSecretKey;
	private String awsSqsQueueEndpoint;
	private String awsSqsQueueUrl;
	private String username;
	private boolean enabled;
	private AbodeSQSInterceptor listener;
	private InterceptorManager interceptorManager;

	@Override
	public void initializePlugin(PluginManager manager, File pluginDirectory) {

		interceptorManager = InterceptorManager.getInstance();
        awsAccessKey = JiveGlobals.getProperty("plugin.abodesqs.aws_access_key", "");
        awsSecretKey = JiveGlobals.getProperty("plugin.abodesqs.aws_secret_key", "");
        awsSqsQueueEndpoint = JiveGlobals.getProperty("plugin.abodesqs.aws_sqs_queue_endpoint", "");
        awsSqsQueueUrl = JiveGlobals.getProperty("plugin.abodesqs.aws_sqs_queue_url", "");
        username = JiveGlobals.getProperty("plugin.abodesqs.username", "");

        // See if the service is enabled or not.
        setEnabled(JiveGlobals.getBooleanProperty("plugin.abodesqs.enabled", false));

        // Listen to system property events
        PropertyEventDispatcher.addListener(this);
    }
	
	private void stopListening(){
		if(listener != null){
			interceptorManager.removeInterceptor(listener);
			listener = null;
		}	
	}
	
	private void startListening(){
		if(listener == null){
			interceptorManager.addInterceptor(listener = new AbodeSQSInterceptor(awsAccessKey,awsSecretKey,awsSqsQueueEndpoint,awsSqsQueueUrl,username));
		}
	}

	@Override
	public void destroyPlugin() {
		stopListening();
	}

    public String getAWSAccessKey() {
        return awsAccessKey;
    }

    public void setAWSAccessKey(String key) {
        JiveGlobals.setProperty("plugin.abodesqs.aws_access_key", key);
        this.awsAccessKey = key;
    }

    public String getAWSSecretKey() {
        return awsSecretKey;
    }

    public void setAWSSecretKey(String secret) {
        JiveGlobals.setProperty("plugin.abodesqs.aws_secret_key", secret);
        this.awsSecretKey = secret;
    }
    
    public String getAWSSQSQueueUrl() {
        return awsSqsQueueUrl;
    }

    public void setAWSSQSQueueUrl(String url) {
        JiveGlobals.setProperty("plugin.abodesqs.aws_sqs_queue_url", url);
        this.awsSqsQueueUrl = url;
    }
    
    public String getAWSSQSQueueEndpoint() {
        return awsSqsQueueEndpoint;
    }

    public void setAWSSQSQueueEndpoint(String endpoint) {
        JiveGlobals.setProperty("plugin.abodesqs.aws_sqs_queue_endpoint", endpoint);
        this.awsSqsQueueEndpoint = endpoint;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String user) {
        JiveGlobals.setProperty("plugin.abodesqs.username", user);
        this.username = user;
    }
    
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        JiveGlobals.setProperty("plugin.abodesqs.enabled",  enabled ? "true" : "false");
        if(this.enabled){
        	startListening();
        }else{
        	stopListening();
        }
    }

	@Override
	public void propertySet(String property, Map<String, Object> params) {
		if (property.equals("plugin.abodesqs.aws_access_key")) {
            this.awsAccessKey = "";
        }
        else if (property.equals("plugin.abodesqs.aws_secret_key")) {
            this.awsSecretKey = "";
        }
        else if (property.equals("plugin.abodesqs.aws_sqs_queue_url")) {
            this.awsSqsQueueUrl = "";
        }
        else if (property.equals("plugin.abodesqs.aws_sqs_queue_endpoint")) {
            this.awsSqsQueueEndpoint = "";
        }
        else if (property.equals("plugin.abodesqs.username")) {
            this.username = "";
        }
	}

	@Override
	public void propertyDeleted(String property, Map<String, Object> params) {
		// Do nothing
	}

	@Override
	public void xmlPropertySet(String property, Map<String, Object> params) {
		// Do nothing
	}

	@Override
	public void xmlPropertyDeleted(String property, Map<String, Object> params) {
		// Do nothing
	}
}